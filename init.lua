require("vis")
local vis = vis

local progname = ...

local M = {prefix = "<Enter>"}  -- ensures hands alternating

local function selection_new_from_motion(keys)
	-- XXX: only motions with one-key mappings are supported:
	if #keys < 1 then return -1 end
	-- XXX: do not consume the count argument:
	if tonumber(keys) then return -1 end
	-- XXX: avoid entering an infinite loop:
	if keys == M.prefix then return #keys end

	for _ = 1, vis.count or 1 do
		local first = vis.win.selections[1].pos
		local last = vis.win.selections[#vis.win.selections].pos
		vis:feedkeys("<vis-mark>a<vis-selections-save><vis-selections-remove-all>")

		-- test the motion direction:
		local pos = vis.win.selection.pos
		vis:feedkeys(keys)
		local forward = vis.win.selection.pos > pos

		-- perform the motion again, this time on the appropriate end of the selection set:
		vis.win.selection.pos = forward and last or first
		vis:feedkeys(keys)

		vis:feedkeys("<vis-mark>a<vis-selections-union>")

		-- for convenience, set the primary selection to either the first or last one,
		-- according to the motion direction:
		if forward then
			vis:feedkeys("<vis-selection-prev>")
		end
	end
	return #keys
end

vis.events.subscribe(vis.events.INIT, function()
	local function h(msg)
		return string.format("|@%s| %s", progname, msg)
	end

	vis:map(vis.modes.NORMAL, M.prefix, selection_new_from_motion, h"Create a new selection at the end of the motion that follows")
end)

return M
